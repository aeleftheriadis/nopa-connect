import React, {Component} from 'react';
import PropTypes from 'prop-types';
import logo from './assets/Logo_Nopa.svg';
import './Header.css';

class Header extends Component {
    static propTypes = {
        hiddenDesktop: PropTypes.bool
    }
    render() {
        let hiddenDesktop = this.props.hiddenDesktop
            ? 'hidden-desktop'
            : '';
        let logoClass = `${hiddenDesktop} Header-logo img`;
        return (
            <div className="Header clearfix">
                <a href="/"><img src={logo} className={logoClass} alt="logo"/></a>
                <a href="/ChooseBank" className="pull-right btn btn-sm btn-pink">Log in</a>
            </div>
        );
    }
}

export default Header;