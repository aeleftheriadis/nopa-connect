// @flow
import React, {Component} from 'react';
import './InputError.css';

class InputError extends Component {
    state = {
        message: 'Input is invalid'
    }
    render() {
        let visible = this.props.visible
            ? 'visible'
            : 'invisible';
        let errorClass = `InputError ${visible}`;
        return (
            <div className={errorClass}>
                <span className="small">{this.props.errorMessage}</span>
            </div>
        );
    }
}

export default InputError;