// @flow
import React, {Component} from 'react';
import Header from './Header';
import SubHeader from './SubHeader';
import Footer from './Footer';
import './Statement.css';

class Statement extends Component {
    constructor(props){
        super(props);
        this.state = {
            transactionsNum: 1
        }
        this.onAddTransactions = this.onAddTransactions.bind(this)
    }
    onAddTransactions(){
        this.setState({
            transactionsNum: this.state.transactionsNum + 1
        });
    }
    render() {
        let transactions =[]
        for (let i = 0; i < this.state.transactionsNum; i += 1) {
            transactions.push(<Transactions key={i} />);
        };
        return (
            <div className="Statement">
                    <Header/>
                    <div className="clear"></div>
                    <SubHeader hiddenMobile={true}/>
                    <div className="clear"></div>
                    <div className="Account-header center-content clearfix">
                        <div className="float-div pull-left perc40">
                            <h3 className="white-text">{this.props.match.params.name}</h3>
                        </div>
                        <div className="float-div pull-right text-right perc60">
                            <h3 className="white-text">Current account<br/>{this.props.match.params.accountNumber}<br/>{this.props.match.params.passCode}</h3>
                        </div>
                    </div>
                    <div className="Account-content center-content clearfix">
                        <p className="body white-text Account-content-header">Your transactions for the last 30 days</p>
                        <TransactionsParent addTransactions={this.onAddTransactions}>
                            {transactions}
                        </TransactionsParent>                        
                    </div>
                    <div className="clear"></div>
                    <Footer hiddenMobile={true}/>
            </div>
        );
    }
}

const TransactionsParent = (props) => (
    <div className="TransactionsParent">    
        {props.children}
        <div className="clear"></div>
        <button className="btn btn-lg btn-pink center-content TransactionsParent-showmore" onClick={props.addTransactions}>Show more</button>                        
    </div>
);

const Transactions = (props) => (
    <div className="Transactions">
        <div className="Transactions-header">
            <p className="small">Today</p>
        </div>
        <div className="Transactions-content">
            <div className="pull-left">
                <p className="small">PAYPAL. ZARA</p>
            </div>
            <div className="pull-right">
                <p className="small">- £35.98</p>
            </div>         
            <div className="clear"></div>  
            <div className="pull-left">
                <p className="small">HOUSE OF FRASER</p>
            </div>
            <div className="pull-right">
                <p className="small">- £35.98</p>
            </div>         
            <div className="clear"></div> 
            <div className="pull-left">
                <p className="small">TESCO</p>
            </div>
            <div className="pull-right">
                <p className="small">- £35.98</p>
            </div>         
            <div className="clear"></div>
        </div>
        <div className="Transactions-header">
            <p className="small">22 November 2016</p>
        </div>
        <div className="Transactions-content">
            <div className="pull-left">
                <p className="small">JOHN LEWIS</p>
            </div>
            <div className="pull-right">
                <p className="small">- £35.98</p>
            </div>         
            <div className="clear"></div>
            <div className="pull-left">
                <p className="small">PAYPAL. ZARA</p>
            </div>
            <div className="pull-right">
                <p className="small">- £35.98</p>
            </div>   
            <div className="clear"></div>
        </div>
        <div className="Transactions-header">
            <p className="small">21 November 2016</p>
        </div>
        <div className="Transactions-content">            
            <div className="pull-left">
                <p className="small">TESCO</p>
            </div>
            <div className="pull-right">
                <p className="small">- £35.98</p>
            </div>         
            <div className="clear"></div>
            <div className="pull-left">
                <p className="small">PAYPAL. ZARA</p>
            </div>
            <div className="pull-right">
                <p className="small">- £35.98</p>
            </div>   
            <div className="clear"></div>
        </div>
        <div className="Transactions-header">
            <p className="small">20 November 2016</p>
        </div>
        <div className="Transactions-content">            
            <div className="pull-left">
                <p className="small">TESCO</p>
            </div>
            <div className="pull-right">
                <p className="small">- £35.98</p>
            </div>    
            <div className="clear"></div>
        </div>
    </div>
);


export default Statement;