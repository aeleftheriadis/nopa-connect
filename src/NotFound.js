// @flow
import React, {Component} from 'react';
import Header from './Header';
import Footer from './Footer';
import './NotFound.css';

class NotFound extends Component {
    render() {
        return (
            <div className="NotFound">
                <Header/>
                <div className="clear"></div>
                <div className="Content spacing center-content clearfix">
                    <h1 className="white-text text-center">Nothing to see here ...</h1>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default NotFound;