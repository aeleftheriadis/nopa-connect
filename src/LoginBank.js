// @flow
import React, {Component} from 'react';
import Header from './Header';
import SubHeader from './SubHeader';
import Footer from './Footer';
import LoginForm from './LoginForm';
import './LoginBank.css';

class LoginBank extends Component {
    render() {
        return (
            <div className="LoginBank">
                <div className="ChooseBank">
                    <Header/>
                    <div className="clear"></div>
                    <SubHeader/>
                    <div className="clear"></div>
                    <div className="small-width center-content clearfix medium-padding">
                        <LoginForm/>
                    </div>
                    <div className="clear"></div>
                    <Footer hiddenMobile={true}/>
                </div>
            </div>
        );
    }
}

export default LoginBank;