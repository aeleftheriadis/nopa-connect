// @flow
import React, {Component} from 'react';
import InputError from './InputError';
import './TextInput.css';

class TextInput extends Component {
  state = {
    isEmpty: true,
    value: '',
    valid: false,
    errorMessage: "",
    errorVisible: false
  }
  handleChange = (e) => {
    this.validation(e.target.value);
    if (this.props.onChange) {
      this
        .props
        .onChange(e);
    }
  }
  validation = (value, valid) => {
    if (typeof valid === 'undefined') {
      valid = true;
    }
    var message = "";
    var errorVisible = false;
    if (!valid) {
      message = this.props.errorMessage;
      valid = false;
      errorVisible = true;
    } else if (this.props.required && value.trim() === "") {
      message = this.props.emptyMessage;
      valid = false;
      errorVisible = true;
    } else if (value.length < this.props.minCharacters) {
      message = this.props.errorMessage;
      valid = false;
      errorVisible = true;
    }
    this.setState({
      value: value,
      isEmpty: value.trim() === "",
      valid: valid,
      errorMessage: message,
      errorVisible: errorVisible
    });

  }
  handleBlur = (e) => {
    var valid = this
      .props
      .validate(e.target.value);
    this.validation(e.target.value, valid);
  }
  render() {
    return (
      <div className="TextInput">
        <input
          name={this.props.name}
          placeholder={this.props.text}
          onChange={this.handleChange}
          onBlur={this.handleBlur}
          value={this.state.value}/>
        <InputError
          visible={this.state.errorVisible}
          errorMessage={this.state.errorMessage}/>
      </div>
    );

  }
}

export default TextInput;