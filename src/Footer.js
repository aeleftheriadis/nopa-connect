import React, {Component} from 'react';
import './Footer.css';
import airbnb from './assets/Airbnb.png';
import metro from './assets/Metro.png';
import pariti from './assets/Pariti.png';
import unshackled from './assets/Unshackled.png';

class Footer extends Component {
    render() {
        let hiddenMobile = this.props.hiddenMobile
            ? 'hidden-mobile'
            : '';
        let preFooterClass = `${hiddenMobile} bg-navy`;
        return (
            <div className="Footer clearfix">
                <div className={preFooterClass}>
                    <div className="full-width center-content clearfix large-padding">
                        <div className="one-fifth-width Footer-logos-same-height">
                            <h3 className="Footer-partners">Our partners:</h3>
                        </div>
                        <FooterIcon logo={airbnb} text="airbnb"/>
                        <FooterIcon logo={metro} text="metro"/>
                        <FooterIcon logo={pariti} text="pariti"/>
                        <FooterIcon logo={unshackled} text="unshackled"/>
                    </div>
                </div>
                <div className="spacing">
                    <div className="full-width center-content copyright">
                        <p className="small white-text text-center">
                            &copy; Zopa Limited 2017 All rights reserved. 'Zopa' and the Zopa logo are trade
                            marks of Zopa Limited. Zopa is a member of CIFAS – the UK's leading anti-fraud
                            association, and we are registered with the Office of the Information
                            Commissioner (No. Z879078).
                        </p>
                        <p className="small white-text text-center">
                            Zopa Limited is incorporated in England &amp; Wales (registration number
                            05197592), with its registered office at 1st Floor, Cottons Centre, Tooley
                            Street, London, SE1 2QG. Zopa Limited is authorised and regulated by the
                            Financial Conduct Authority, and entered on the Financial Services Register
                            under firm registration number 563134.
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

const FooterIcon = (props) => (
    <div className="one-fifth-width Footer-logos-same-height">
        <div className="img-container">
            <div className="centerer"></div>
            <img src={props.logo} alt={props.text}/>
        </div>
    </div>
);

export default Footer;