// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Home from './Home';
import ChooseBank from './ChooseBank';
import LoginBank from './LoginBank';
import Statement from './Statement';
import NotFound from './NotFound';

ReactDOM.render(
  <Router>
  <Switch>
    <Route exact path="/" component={Home}/>
    <Route path="/choosebank" component={ChooseBank}/>
    <Route path="/loginBank/:bank" component={LoginBank}/>
    <Route path="/statement/:name/:accountNumber/:passCode" component={Statement}/>
    <Route component={NotFound}/>
  </Switch>
</Router>,
document.getElementById('root'));
