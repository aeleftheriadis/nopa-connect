// @flow
import React, {Component} from 'react';
import './SubHeader.css';

class SubHeader extends Component {
    render() {
        let hiddenMobile = this.props.hiddenMobile
            ? 'hidden-mobile'
            : '';
        let subheaderClass = `${hiddenMobile} SubHeader`;
        return (
            <div className={subheaderClass}>
                <div className="Content spacing clearfix no-bottom-spacing">
                    <div className="medium-width center-content">
                        <h2 className="center-content text-center white-text">Which bank does this account belong to?</h2>
                        <p
                            className="body center-content text-center white-text SubHeader-body-text-padding">
                            Track of all your payments by connecting as many bank accounts as you’d like to
                            your Nopa account and get updates your balance instantly. Plus it’s free.
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

export default SubHeader;