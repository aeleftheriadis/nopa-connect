// @flow
import React, {Component} from 'react';
import {Redirect} from 'react-router-dom'
import TextInput from './TextInput';
import InputError from './InputError';
import './LoginForm.css';

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            surname: '',
            sortCode: '',
            accountNumber: '',
            passcode: '',
            memorableWord: '',
            errorVisible: false,
            errorMessage: 'Please fill all fields',
            submitForm: false
        };
        this.handleSubmit = this
            .handleSubmit
            .bind(this);
    }

    handleSubmit = (e) => {
        e.preventDefault();
        if (this.state.surname.trim() === "" || this.state.sortCode.trim() === "" || this.state.accountNumber.trim() === "" || this.state.passcode.trim() === "" || this.state.memorableWord.trim() === "") {
            this.setState({errorVisible: true})
            return;
        } else {
            this.setState({errorVisible: false, submitForm: true})
        }
    }
    commonValidate() {
        return true;
    }
    shortcodeValidate(value) {
        var regex = /^(\d){2}-(\d){2}-(\d){2}$/;
        return regex.test(value);
    }
    accountNumberValidate(value) {
        var regex = /^(\d){7,8}$/;
        return regex.test(value);
    }
    handleInputChange = (e) => {
        const target = e.target;
        const value = target.value;
        const name = target.name;
        this.setState({[name]: value});
    }
    render() {
        if (this.state.submitForm) {
            const route = `/Statement/${this.state.surname}/${this.state.accountNumber}/${this.state.passcode}`
            return (<Redirect to={route}/>)
        } else {
            return (
                <form onSubmit={this.handleSubmit}>
                    <TextInput
                        name="surname"
                        text="Surname"
                        required={true}
                        validate={this.commonValidate}
                        onChange={this.handleInputChange}
                        errorMessage="Surname is invalid"
                        emptyMessage="Surname is required"/>
                    <TextInput
                        name="sortCode"
                        text="Sort code"
                        required={true}
                        validate={this.shortcodeValidate}
                        onChange={this.handleInputChange}
                        errorMessage="Sort code is invalid"
                        emptyMessage="Sort code is required"/>
                    <TextInput
                        name="accountNumber"
                        text="Account number"
                        required={true}
                        validate={this.accountNumberValidate}
                        onChange={this.handleInputChange}
                        errorMessage="Account number is invalid"
                        emptyMessage="Account number is required"/>
                    <TextInput
                        name="passcode"
                        text="Passcode"
                        required={true}
                        minCharacters={6}
                        validate={this.commonValidate}
                        onChange={this.handleInputChange}
                        errorMessage="Passcode is invalid"
                        emptyMessage="Passcode is required"/>
                    <TextInput
                        name="memorableWord"
                        text="Memorable word"
                        required={true}
                        validate={this.commonValidate}
                        onChange={this.handleInputChange}
                        errorMessage="Memorable word is invalid"
                        emptyMessage="Memorable word is required"/>
                    <InputError
                        visible={this.state.errorVisible}
                        errorMessage={this.state.errorMessage}/>
                    <button className="btn btn-lg btn-pink LoginForm-btn">Log in &amp; connect</button>

                </form>
            );
        }
    }
}

export default LoginForm;