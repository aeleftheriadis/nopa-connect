// @flow
import React, {Component} from 'react';
import './Home.css';
import Header from './Header';
import Footer from './Footer';
import logo from './assets/Logo_Nopa.svg';
import shapes from './assets/Shapes.svg';

class Home extends Component {
    render() {
        return (
            <div className="Home">
                <Header hiddenDesktop={true}/>
                <div className="clear"></div>
                <div className="Content spacing clearfix">
                    <img src={logo} className="hidden-mobile center-content img" alt="logo"/>
                    <h1 className="home-heading center-content text-center">Your finances, in one place</h1>
                    <p className="home-subtitle center-content text-center">
                        Track of all your payments by connecting as many bank accounts as you’d like to
                        your Nopa account and get updates your balance instantly.
                    </p>
                    <p className="center-content text-center">
                        <a href="/ChooseBank" className="btn btn-lg btn-pink ">Get started</a>
                    </p>
                </div>
                <div className="clear"></div>
                <div className="Content spacing clearfix bg-white no-bottom-spacing">
                    <div className="full-width center-content clearfix medium-padding">
                        <div className="half-width bottom">
                            <h1 className="blue-text main-heading">There’s no such things as “one size fits all” finance.</h1>
                            <p className="body">
                                We were founded to make money simple and fair, for everyone: whether you’re
                                looking for a loan, or to get better rewards for your investments.
                            </p>
                        </div>
                        <div className="half-width">
                            <img
                                src={shapes}
                                className="center-content img pull-right home-shape"
                                alt="shapes"/>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
}

export default Home;