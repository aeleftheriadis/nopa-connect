// @flow
import React, {Component} from 'react';
import Header from './Header';
import SubHeader from './SubHeader';
import Footer from './Footer';
import InputError from './InputError';
import './ChooseBank.css';
import barclays from './assets/Barclays.png';
import natwest from './assets/LogoNatwest.png'
import lloyds from './assets/LogoLloyds.png'
import hsbc from './assets/LogoHSBC.png'
import tsb from './assets/LogoTSB.png'
import santander from './assets/LogoSantander.png'

class ChooseBank extends Component {
    constructor(props) {
        super(props)
        this.state = {
            selectedLogo: null,
            errorVisible: false,
            errorMessage: 'Please select bank'
        }
        this.handleAnchorCick = this
            .handleAnchorCick
            .bind(this);
    }
    state = {
        selectedLogo: null,
        errorVisible: false,
        errorMessage: 'Please select bank'
    }
    handleClick = (e) => {
        this.setState({selectedLogo: e.target.alt, errorVisible: false})
    }
    handleAnchorCick = (e) => {
        if (this.state.selectedLogo === null) {
            e.preventDefault();
            this.setState({errorVisible: true})
        } else 
            return;
        }
    render() {
        let url = `/LoginBank/${this.state.selectedLogo}`;
        return (
            <div className="ChooseBank">
                <Header/>
                <div className="clear"></div>
                <SubHeader/>
                <div className="clear"></div>
                <div className="small-width center-content clearfix medium-padding">
                    <BankLogo
                        logo={barclays}
                        text="barclays"
                        border={this.state.selectedLogo}
                        click={this.handleClick}/>
                    <BankLogo
                        logo={natwest}
                        text="natwest"
                        border={this.state.selectedLogo}
                        click={this.handleClick}/>
                    <BankLogo
                        logo={lloyds}
                        text="lloyds"
                        border={this.state.selectedLogo}
                        click={this.handleClick}/>
                    <BankLogo
                        logo={hsbc}
                        text="hsbc"
                        border={this.state.selectedLogo}
                        click={this.handleClick}/>
                    <BankLogo
                        logo={tsb}
                        text="tsb"
                        border={this.state.selectedLogo}
                        click={this.handleClick}/>
                    <BankLogo
                        logo={santander}
                        text="santander"
                        border={this.state.selectedLogo}
                        click={this.handleClick}/>
                    <InputError
                        visible={this.state.errorVisible}
                        errorMessage={this.state.errorMessage}/>
                    <a
                        href={url}
                        className="btn btn-lg btn-pink Connect-btn"
                        onClick={this.handleAnchorCick}>Connect</a>
                </div>
                <div className="clear"></div>
                <Footer hiddenMobile={true}/>
            </div>
        );
    }
}

class BankLogo extends Component {
    render() {
        let selectedClass = "img-container";
        selectedClass += this.props.border === this.props.text
            ? ' selected'
            : '';
        return (
            <div className="half-width BankLogo">
                <div className={selectedClass}>
                    <div className="centerer"></div>
                    <img
                        className="img"
                        src={this.props.logo}
                        alt={this.props.text}
                        onClick={this.props.click}/>
                </div>
            </div>
        );
    }
}

export default ChooseBank;
